```bash
# Detection
# ‾‾‾‾‾‾‾‾‾

hook global BufCreate .*[.](mth|math) %{
  set-option buffer filetype math
}
```
  
NOTE: primary use-case is for the snippets in `math/` directory, then rename your working file to its required extension `*.ext`.  
@see snippet libraries; [occivink](https://github.com/occivink/kakoune-snippets) and [andreyorst](https://github.com/andreyorst/kakoune-snippet-collection), or [alexherbo2](https://github.com/alexherbo2/snippets.kak).  
  

---


#### Usage 

  
Definition: An integer 'n' is even if there exists another integer 't' such that n = 2t  
  
n (setElementOf) (letterInteger) (logicalAnd) n is even (ifAndOnlyIf) (thereExists) t (setElementOf) (letterInteger) s.t. n = 2t  
  
n ∈ ℤ ∧ n is even ⇔ ∃ t ∈ ℤ s.t. n = 2t  
  

---


#### Install

  
```
mv math.kak ~/.config/kak/autoload/math.kak
```
  
[plug.kak](https://github.com/andreyorst/plug.kak)
```
plug "KJ_Duncan/kakoune-math.kak" domain "bitbucket.org"
``` 

---


Enable snippets via a symbolic-link:  
  
```bash
$ ln -s plugins/kakoune-math.kak/math plugins/snippet.kak/snippets/
$ ln -s plugins/kakoune-math.kak/math plugins/kakoune-snippet-collection/snippets/
```
  
  
