# --------------------------------------------------------------------------------------------------- #
# https://unicode-table.com/en/
# https://unicode-table.com/en/sets/mathematical-signs/
# https://www.fileformat.info/info/unicode/block/mathematical_operators/list.htm
# --------------------------------------------------------------------------------------------------- #
# kak colour codes; value:red, type,operator:yellow, variable,module,attribute:green,
#                   function,string,comment:cyan, keyword:blue, meta:magenta, builtin:default
# --------------------------------------------------------------------------------------------------- #

# Detection
# ‾‾‾‾‾‾‾‾‾

hook global BufCreate .*[.](mth|math) %{
  set-option buffer filetype math
}

# Initialization
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾

hook global WinSetOption filetype=math %{
  require-module math

  set-option window static_words %opt{math_static_words}

  set-option buffer comment_line '#'
  set-option buffer comment_block_begin '/*'
  set-option buffer comment_block_end '*/'

  hook -once -always window WinSetOption filetype=.* %{ remove-hooks window text.+ }
}

# --------------------------------------------------------------------------------------------------- #
hook -group plantuml-highlight global WinSetOption filetype=math %{
  add-highlighter window/math ref math
  hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/math }
}

# --------------------------------------------------------------------------------------------------- #
provide-module math %§

add-highlighter shared/math regions
add-highlighter shared/math/code default-region group
add-highlighter shared/math/string region '"' (?<!\\)(\\\\)*" fill string
add-highlighter shared/math/comment region '#' '$' fill comment
add-highlighter shared/math/block region '/\*' '\*/' fill comment

# --------------------------------------------------------------------------------------------------- #
#
evaluate-commands %sh{
  exec awk -f - <<'EOF'
  BEGIN{
  split("approxEqualTo approxEquals approxNotEqualTo arrowLeft arrowRight dot endOfProof "\
        "equalToNot equalToOrGreaterThan equalToOrLessThan forAll greekCapitalDelta "\
        "greekCapitalLamda greekCapitalOmega greekCapitalPi greekCapitalRho "\
        "greekCapitalSigma greekSmallAplha greekSmallBeta greekSmallDelta "\
        "greekSmallEpsilon greekSmallGamma greekSmallLamda greekSmallOmega greekSmallPi "\
        "greekSmallSigma greekSmallTheta ifAndOnlyIf infinity integralsDouble integralsIntegral "\
        "ceilingBracketLeft floorBracketLeft ceilingBracketRight floorBracketRight "\
        "integralsTriple letterComplex letterInteger letterNatural letterRational "\
        "letterReal letterSmallF logicalAnd logicalBecause logicalBiconditional "\
        "logicalEquivalent logicalEquivalentTo logicalEquivalentToNot logicalImplies "\
        "logicalImpliesLeft logicalImpliesRight logicalNot logicalOr logicalProportion "\
        "logicalTherefore nAryCoproduct nAryProduct nArySummation rootCubed rootSquared "\
        "setComplement setElementOf setElementOfNot setEmpty setIntersection setSubset "\
        "setSubsetOf setSubsetOfNot setUnion subscriptA subscriptE subscriptEight "\
        "subscriptEquals subscriptFive subscriptFour subscriptH subscriptK subscriptL "\
        "subscriptLeftParenthesis subscriptM subscriptMinus subscriptN subscriptNine "\
        "subscriptO subscriptOne subscriptP subscriptPlus subscriptRightParenthesis "\
        "subscriptS subscriptSeven subscriptSix subscriptT subscriptThree subscriptTwo "\
        "subscriptX subscriptZero superscriptEight superscriptEquals superscriptFive "\
        "superscriptFour superscriptI superscriptLeftParenthesis superscriptMinus "\
        "superscriptN superscriptNine superscriptOne superscriptPlus "\
        "superscriptRightParenthesis superscriptSeven superscriptSix superscriptThree "\
        "superscriptTwo superscriptZero thereExists", words);
  }

  function print_words(words) {
    for (i in words) { printf(" %s", words[i]); }
  }

  BEGIN {
    printf("declare-option str-list math_static_words ");
    print_words(words);
    printf("\n");
  }
EOF
}
# --------------------------------------------------------------------------------------------------- #
§
